#
# Makefile used to operate on-demand service via the docker-compose CLI tool
#

# Define default shell to use
SHELL=/bin/bash

# Required ENV variables
WORKDIR = $(shell pwd)
DOCKER_HOST = tcp://127.0.0.1:2376
DOCKER_TLS_VERIFY = 1
DOCKER_COMPOSE_ENV = phpmyadmin.env
DOCKER_COMPOSE_FILE = docker-compose.yml
DOCKER_COMPOSE_CLI = docker-compose --env-file $(WORKDIR)/$(DOCKER_COMPOSE_ENV) --file $(WORKDIR)/$(DOCKER_COMPOSE_FILE)

environment-configuration:
	cd $(WORKDIR); \
	export DOCKER_HOST=$(DOCKER_HOST); \
	export DOCKER_TLS_VERIFY=$(DOCKER_TLS_VERIFY);

# The export directive takes a variable and makes it accessible to sub-make commands
export DOCKER_HOST DOCKER_TLS_VERIFY

status: environment-configuration
	$(DOCKER_COMPOSE_CLI) ps;

build: environment-configuration
	$(DOCKER_COMPOSE_CLI) build;

up: environment-configuration
	$(DOCKER_COMPOSE_CLI) up -d;

up-force: environment-configuration
	$(DOCKER_COMPOSE_CLI) up -d --force-recreate;

start: environment-configuration
	$(DOCKER_COMPOSE_CLI) start;

stop: environment-configuration
	$(DOCKER_COMPOSE_CLI) stop --timeout 30;

restart: environment-configuration
	$(DOCKER_COMPOSE_CLI) restart;

logs: environment-configuration
	$(DOCKER_COMPOSE_CLI) logs -f --tail=50;

top: environment-configuration
	$(DOCKER_COMPOSE_CLI) top;

rm: environment-configuration
	$(DOCKER_COMPOSE_CLI) rm --stop -v;
