# Run a PhpMyAdmin "On Demand"

This code repository allows you to start a [PhpMyAdmin](https://www.phpmyadmin.net) instance __"On Demand"__.  
Sometimes, you may need to manage your databases or tables on a remote server.  

Of course, you can do it with the (MySQL|MariaDB)'s CLI but, maybe as lazy guy like me, to clik on a IHM can be more faster (or not) than write SQL commands over SSH.  
If you're in that case, this repository is maybe made for you !  

## Why

Because, with this `docker-compose.yml` and a little bit of `Traefik` configuration, it's possible to start __"on demand"__ PhpMyAdmin without to expose the port of your (MySQL|MariaDB) server on the Internets.
If you don't want to play with [Traefik](https://gitlab.com/jlecorre/docker-traefik), I advise you to consult the `apache2-config` tag available from the repository.  
This tag allows you to expose PhpMyAdmin via an Apache2 reverse proxy configuration.  

## Why "on demand"

It's simple, the PhpMyAdmin project can be impacted by some [security vulnerabilities](https://www.cvedetails.com/vendor/784/Phpmyadmin.html), so leave accessible a domain name like `http://phpmyadmin.domain-name.tld` on the Internets can be considered like a vulnerability in your security policy.  

With this `docker-compose.yml` you will be able to `start` and `stop` "on demand" your PhpMyAdmin instance.  
When the Docker container is stopped, an attacker will get an error like `Service Unavailable` or `404 page not found` depending on your setup.  

## How it works

* Configure your Traefik instance directly from this repository: [jlecorre/docker-traefik](https://gitlab.com/jlecorre/docker-traefik)
* Checkout this repository
* Update the `phpmyadmin.env` file available in the repository
* Start the Docker container through the `docker-compose` command line

```bash
docker-compose --env-file phpmyadmin.env -f docker-compose.yml up -d [--build] [--force-recreate]
```

* If you want to stop your "on demand" instance of PhpMyAdmin  

```bash
docker-compose --env-file phpmyadmin.env -f docker-compose.yml stop
```

* If you want to (re)start your "on demand" instance of PhpMyAdmin  

```bash
docker-compose --env-file phpmyadmin.env -f docker-compose.yml start
```

* To rebuild this image   

```bash
docker-compose --env-file phpmyadmin.env build or docker-compose up --build
docker-compose --env-file phpmyadmin.env up --build --force-recreate
```

## Useful informations

### Makefile usage
A `Makefile` is available in this repository to easily operate the Docker Compose service

### About the docker image builded from docker-compose

### About MySQL and MariaDB server
Depending of your databases server, after the version `5.5.7` of MySQL *(MariaDB is also based on)*, [some authentication plugins had been integrated](https://dev.mysql.com/doc/refman/5.6/en/authentication-plugins.html).  
So, to enable remote connection and avoid error `#1698 - Access denied for user 'username'@'localhost'`, you will need to create a new user with a specific configuration tailored to your needs.  

To see what type of plugins your MySQL/MariaDB users use, you need to run the following command lines :  
```shell
mysql> USE mysql;
mysql> SELECT User, Host, plugin FROM mysql.user;
```

To autorize a MySQL user to open a connection from dockerized PhpMyADmin, you have 2 available solutions :  
* create a __non root__ user with the `mysql_native_password` plugin configured and setting up the useful grant options to administrate your databases  
* update the current root user to set `mysql_native_password` plugin instead of `unix_socket` plugin and flush privileges  

Of course, the first solution is better to use for security reason but here are some command lines to setting up the second one :  
```
use mysql;
update user set plugin='mysql_native_password' where User='root';
flush privileges;
```

## Useful sources
https://hub.docker.com/r/phpmyadmin/phpmyadmin  
https://docs.phpmyadmin.net/en/latest/index.html  
https://stackoverflow.com/questions/39281594/error-1698-28000-access-denied-for-user-rootlocalhost  
https://superuser.com/questions/603026/mysql-how-to-fix-access-denied-for-user-rootlocalhost  

## Requirements
* Get and install [Docker](https://docs.docker.com/engine/installation/) on your working environment
* Get and install [docker-compose](https://docs.docker.com/compose/install/) on your working environment

That's all ! So enjoy :)
